import firebase from 'firebase'
import firestore from 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyDXUXBY59fn6C40Fl57QnH1o_e8PXrZI80",
    authDomain: "smoothies-4ac08.firebaseapp.com",
    databaseURL: "https://smoothies-4ac08.firebaseio.com",
    projectId: "smoothies-4ac08",
    storageBucket: "smoothies-4ac08.appspot.com",
    messagingSenderId: "270379732876",
    appId: "1:270379732876:web:9c2f2a0a291974095110a9",
    measurementId: "G-G3WKRDZQKN"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore()



