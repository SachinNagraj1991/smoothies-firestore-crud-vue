import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import AddSmoothie from '@/components/addSmoothie'
import EditSmoothie from '@/components/editSmoothie'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/addSmoothie',
      name: 'addSmoothie',
      component: AddSmoothie
    },
    {
      path: '/editSmoothie/:id/:title',
      name: 'editSmoothie',
      component: EditSmoothie
    }
  ]
})
